package com.testgames.quizapp.backend;

import com.testgames.quizapp.backend.filters.StatelessAuthenticationFilter;
import com.testgames.quizapp.backend.services.authentication.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletResponse;

@EnableWebSecurity
class SecurityConfig {
    @Configuration
    @Order(1)
    public static class MVCWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        private UserDetailsService userDetailsService;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring()
                    .antMatchers("/css/**")
                    .antMatchers("/js/**");
        }

        @Bean
        public BCryptPasswordEncoder bCryptPasswordEncoder() {
            return new BCryptPasswordEncoder();
        }

        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable() //TODO fix later
                    .authorizeRequests()
                        .antMatchers("/*").hasRole("USER")
                        .antMatchers("/Admin/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.GET, "/API/contact/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.PATCH, "/API/contact/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.DELETE, "/API/contact/**").hasRole("ADMIN")

                        .antMatchers(HttpMethod.GET, "/API/users/getByIDAdmin/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.GET, "/API/users/getAdmins/**").hasRole("ADMIN")

                        .antMatchers(HttpMethod.POST, "/API/Questions/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.DELETE, "/API/Questions/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.PATCH, "/API/Questions/**").hasRole("ADMIN")
                        .antMatchers(HttpMethod.GET, "/API/Questions/*").hasRole("ADMIN")

                        .antMatchers(HttpMethod.PATCH, "/API/users/MP/**").hasAuthority("ROLE_MPSERVER")
                        .antMatchers(HttpMethod.PATCH, "/API/users/updateUserAdmin/**").hasRole("ADMIN")

                        .and()
                    .formLogin()
                        .loginPage("/login")
                        .failureUrl("/login?error") //Explicit is better than implicit ;)
                        .permitAll()
                        .and()
                    .logout()
                        .permitAll();}
    }

    @Configuration
    @Order(2)
    public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Autowired
        TokenService tokenService;

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring()
                    .antMatchers("/API/authentication/**");
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable() //Not needed on API
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//                    .exceptionHandling().authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized"))
//                    .and()
//                    .anonymous().disable()
//                    .servletApi().and()
//                    .headers().cacheControl().and().and()
//                    .authorizeRequests()

                    .authorizeRequests()
                    .antMatchers(HttpMethod.POST, "/API/contact/**").authenticated()

                    .antMatchers(HttpMethod.GET, "/API/users/getUserByID/**").authenticated()
                    .antMatchers(HttpMethod.PATCH, "/API/users/**").authenticated()
                    .antMatchers(HttpMethod.GET, "/API/users/**").authenticated()
                    .antMatchers(HttpMethod.DELETE, "/API/users/**").authenticated()

                    .antMatchers(HttpMethod.GET, "/API/Questions").authenticated()
                    .and()
                    .addFilterBefore(new StatelessAuthenticationFilter(tokenService), UsernamePasswordAuthenticationFilter.class);
        }
    }
}