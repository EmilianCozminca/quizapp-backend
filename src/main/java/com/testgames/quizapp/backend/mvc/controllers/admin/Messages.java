package com.testgames.quizapp.backend.mvc.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class Messages {
	
	@RequestMapping("Admin/Messages")
	public String messages(Model model) {
		return "AdminMessages";
	}
}
