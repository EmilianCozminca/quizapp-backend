package com.testgames.quizapp.backend.mvc.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class Questions {
	@RequestMapping("Admin/Questions")
	public String questions(Model model) {
		return "AdminQuestions";
	}
}
