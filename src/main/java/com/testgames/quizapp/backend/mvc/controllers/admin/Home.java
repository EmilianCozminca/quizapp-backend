package com.testgames.quizapp.backend.mvc.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
class Home {
	@RequestMapping("Admin")
	public String home(Model model) {
		return "AdminHome";
	}
}
