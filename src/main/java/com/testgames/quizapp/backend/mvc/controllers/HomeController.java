package com.testgames.quizapp.backend.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
class HomeController {
	@RequestMapping("/")
	public String questions(HttpServletRequest request, Model model) {
		
		if (request.isUserInRole("ROLE_ADMIN"))
			return "redirect:/Admin";

		return "Home";
	}
}
