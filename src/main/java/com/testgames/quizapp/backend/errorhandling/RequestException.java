package com.testgames.quizapp.backend.errorhandling;

import java.io.IOException;

public class RequestException extends IOException {
	private static final long serialVersionUID = 1L;
	
	RequestException() {
	}
	
	public RequestException(String message) {
		super(message);
	}
}