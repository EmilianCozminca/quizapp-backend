package com.testgames.quizapp.backend.errorhandling;

class ResourceNotFoundException extends RequestException {
	private static final long serialVersionUID = 1L;
	
	public ResourceNotFoundException() {
		super("The requested resource could not be found.");
	}
	
	public ResourceNotFoundException(String message) {
		super(message);
	}
}
