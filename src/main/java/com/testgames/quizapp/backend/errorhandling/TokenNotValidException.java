package com.testgames.quizapp.backend.errorhandling;

public class TokenNotValidException extends RequestException {
	private static final long serialVersionUID = 1L;
	
	public TokenNotValidException() {
		super("Invalid token.");
	}
	
	public TokenNotValidException(String message) {
		super(message);
	}
}