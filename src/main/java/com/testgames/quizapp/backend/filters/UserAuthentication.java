package com.testgames.quizapp.backend.filters;

import com.testgames.quizapp.backend.model.entities.Role;
import com.testgames.quizapp.backend.model.entities.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class UserAuthentication implements Authentication {
    private final Long userId;
    private final Set<GrantedAuthority> roles = new HashSet<>();
    private boolean authenticated = true;

    public UserAuthentication(Long userId, List<Role> roles) {
        this.userId = userId;

        for (Role r : roles)
            this.roles.add(new SimpleGrantedAuthority(r.getName()));
    }

    @Override
    public String getName() {
        return Long.toString(userId);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public User getDetails() {
        return null;
    }

    @Override
    public String getPrincipal() {
        return Long.toString(userId);
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
