package com.testgames.quizapp.backend.filters;

import com.testgames.quizapp.backend.errorhandling.TokenNotValidException;
import com.testgames.quizapp.backend.model.entities.Role;
import com.testgames.quizapp.backend.services.authentication.TokenService;
import com.testgames.quizapp.backend.services.authentication.UserAuthDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

public class StatelessAuthenticationFilter extends GenericFilterBean {

    private final TokenService tokenService;

    public StatelessAuthenticationFilter(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Authentication authentication = getAuthentication(httpRequest);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private Authentication getAuthentication(HttpServletRequest request) throws TokenNotValidException {
        final String token = request.getHeader("Authorization");

        if (token != null) {
            String[] str = token.split("\\s+");
            if (str[0].compareTo("Bearer") != 0)
                throw new TokenNotValidException("Invalid Authorization Header");

            UserAuthDetails userAuthDetails = tokenService.decodeAccessToken(str[1]);

            Long id = userAuthDetails.getId();
            List<Role> roles = userAuthDetails.getRoles();

            return new UserAuthentication(id, roles);
        }
        return null;
    }
}