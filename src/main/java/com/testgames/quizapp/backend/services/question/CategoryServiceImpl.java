package com.testgames.quizapp.backend.services.question;

import com.testgames.quizapp.backend.model.entities.Category;
import com.testgames.quizapp.backend.model.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private
	CategoryRepository categoryRepository;
	
	@Override
	public void post(Category category) {
		categoryRepository.save(category);
	}
	
	@Override
	public List<Category> getAll() {
		return categoryRepository.findAll();
	}
	
	@Override
	public Category getOne(long id) {
		return categoryRepository.findOne(id);
	}
	
	@Override
	public void delete(long id) {
		categoryRepository.delete(id);
	}
}
