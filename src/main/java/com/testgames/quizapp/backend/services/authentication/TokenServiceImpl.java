package com.testgames.quizapp.backend.services.authentication;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.testgames.quizapp.backend.errorhandling.TokenNotValidException;
import com.testgames.quizapp.backend.model.entities.RefreshToken;
import com.testgames.quizapp.backend.model.entities.Role;
import com.testgames.quizapp.backend.model.entities.User;
import com.testgames.quizapp.backend.model.repositories.RefreshTokenRepository;
import com.testgames.quizapp.backend.model.repositories.UserRepository;
import io.jsonwebtoken.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.apache.http.HttpHeaders.USER_AGENT;

@Component
public class TokenServiceImpl implements TokenService {
	private final GoogleIdTokenVerifier o_GoogleIDTokenVerifier;
	private final HttpClient client = HttpClientBuilder.create().build();
	@Autowired
	UserRepository userRepository;
	@Autowired
	private
	RefreshTokenRepository refreshTokenRepository;
	//Configuration
	@Value("${token.secret}")
	private String apiKEY;
	@Value("${token.audience}")
	private String audience;
	@Value("${token.issuer}")
	private String issuer;
	@Value("${token.accessTokenLifetimeMinutes}")
	private int accesTokenLifetimeMinutes;
	@Value("${token.refreshTokenLifetimeYears}")
	private int refreshTokenLifetimeDays;
	@Value("${facebook.graph.login}")
	private String graphAPI;
	
	//Properties are loaded after constructor, this is a workaround.
	@Autowired
	public TokenServiceImpl(@Value("${token.serverClientId}") String serverClientId) {
		
		o_GoogleIDTokenVerifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
				//.setAudience(Collections.singletonList(serverClientId)) // TODO remove when in production
				//.setIssuer("https://accounts.google.com") //TODO fix that (https and http)
				.build();
	}
	
	@Override
	public SocialLoginDetails getFacebookDetails(String facebookToken) throws IOException {
		HttpGet request = new HttpGet(graphAPI + facebookToken);
		request.addHeader("User-Agent", USER_AGENT);
		HttpResponse response = client.execute(request);
		
		SocialLoginDetails facebookLoginDetails = new SocialLoginDetails();
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		
		StringBuilder result = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		if (result != null) {
			ObjectNode object = new ObjectMapper().readValue(result.toString(), ObjectNode.class);
			JsonNode nodeId = object.get("id");
			JsonNode nodeEmail = object.get("email");
			JsonNode displayName = object.get("name");
			
			if (nodeId == null || nodeEmail == null || displayName == null)
				throw new TokenNotValidException("Malformed Facebook token. Try again.");
			
			facebookLoginDetails.setID(nodeId.textValue());
			facebookLoginDetails.setEmail(nodeEmail.textValue());
			facebookLoginDetails.setDisplayName(displayName.textValue());
			
			return facebookLoginDetails;
		}
		throw new TokenNotValidException("Cannot connect to Graph API.");
	}
	
	@Override
	public SocialLoginDetails getGoogleDetails(String googleTOKEN) throws TokenNotValidException {
		SocialLoginDetails googleLoginDetails = new SocialLoginDetails();
		try {
			GoogleIdToken idToken = o_GoogleIDTokenVerifier.verify(googleTOKEN);
			
			if (idToken == null)
				throw new TokenNotValidException();
			
			googleLoginDetails.setID(idToken.getPayload().getSubject());
			googleLoginDetails.setEmail(idToken.getPayload().getEmail());
			googleLoginDetails.setDisplayName(idToken.getPayload().get("name").toString());
			
			return googleLoginDetails;
			
		} catch (GeneralSecurityException | IOException | IllegalArgumentException e) {
			throw new TokenNotValidException("Cannot validate. Please generate a new Google TOKEN.");
		}
		
	}
	
	@Override
	public String createRefreshTokenForUser(User user) {
		Date date = new Date();
		
		RefreshToken refreshToken = new RefreshToken();
		refreshToken.setUser(user);
		user.getRefreshTokens().add(refreshToken);
		
		refreshTokenRepository.save(refreshToken);
		
		//Adding tokenlife years to token lifetime, JJWT doesn't support LocalDateTime
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, refreshTokenLifetimeDays);
		Date expDate = calendar.getTime();
		//
		
		return Jwts.builder().setIssuedAt(date)
				.setExpiration(expDate)
				.setSubject(Long.toString(user.getId()))
				.setAudience(audience)
				.setIssuer(issuer)
				.setId(refreshToken.getToken())
				.claim("scopes", "ROLE_REFRESH_TOKEN") //TODO check
				.signWith(SignatureAlgorithm.HS256, apiKEY)
				.compact();
	}
	
	@Override
	public String createAccessTokenForUser(User user) {
		Date date = new Date();
		ObjectMapper mapper = new ObjectMapper();
		
		//Adding tokenlife years to token lifetime, JJWT doesn't support LocalDateTime
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, accesTokenLifetimeMinutes);
		Date expDate = calendar.getTime();
		//
		
		try {
			return Jwts.builder().setIssuedAt(date)
					.setExpiration(expDate)
					.setSubject(Long.toString(user.getId()))
					.setAudience(audience)
					.setIssuer(issuer)
					.claim("roles", mapper.writeValueAsString(user.getRoles())) //TODO check
					.signWith(SignatureAlgorithm.HS256, apiKEY)
					.compact();
		} catch (JsonProcessingException e) {
			//Shouldn't happen.
			return null;
		}
	}
	
	@Override
	public UserAuthDetails decodeAccessToken(String encodedToken) throws TokenNotValidException {
		//Create a map that contains the ID of the user and its roles.
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		
		UserAuthDetails userAuthDetails = new UserAuthDetails();
		
		Claims claims;
		try {
			claims = Jwts.parser()
					.setSigningKey(getSigningKey())
					.requireAudience(audience)
					.requireIssuer(issuer)
					.parseClaimsJws(encodedToken)
					.getBody();
		} catch (IllegalArgumentException | MalformedJwtException e) {
			throw new TokenNotValidException("Malformed JWT. Please login again.");
		} catch (SignatureException e) {
			throw new TokenNotValidException("Malformed JWT signature.");
		} catch (ExpiredJwtException e) {
			throw new TokenNotValidException("Expired JWT. Please send the refresh token.");
		}
		
		try {
			userAuthDetails.setId(Long.parseLong(claims.get("sub").toString()));
			userAuthDetails.setRoles(mapper.readValue(claims.get("roles").toString(), new TypeReference<List<Role>>() {}));
		} catch (IOException e) {
			throw new TokenNotValidException("Something went wrong :\\");
		}
		return userAuthDetails;
	}
	
	//TODO make reusable code(Over engineering, not a priority)
	@Override
	public User decodeRefreshToken(String encodedToken) throws TokenNotValidException {
		Claims claims;
		
		try {
			claims = Jwts.parser()
					.setSigningKey(getSigningKey())
					.requireAudience(audience)
					.requireIssuer(issuer)
					.parseClaimsJws(encodedToken)
					.getBody();
		} catch (IllegalArgumentException | MalformedJwtException e) {
			throw new TokenNotValidException("Malformed JWT. Please login again.");
		} catch (SignatureException e) {
			throw new TokenNotValidException("Malformed JWT signature.");
		} catch (ExpiredJwtException e) {
			throw new TokenNotValidException("Expired JWT. Please login again.");
		}
		
		RefreshToken refreshToken = refreshTokenRepository.findOne(claims.getId());
		
		if (refreshToken == null || refreshToken.isBlocked())
			throw new TokenNotValidException("User logged out or the refresh token had been renewed.");
		
		return refreshToken.getUser();
	}
	
	@Override
	public String decodeRefreshTokenToString(String encodedToken) throws TokenNotValidException {
		Claims claims;
		
		try {
			claims = Jwts.parser()
					.setSigningKey(getSigningKey())
					.requireAudience(audience)
					.requireIssuer(issuer)
					.parseClaimsJws(encodedToken)
					.getBody();
		} catch (IllegalArgumentException | MalformedJwtException e) {
			throw new TokenNotValidException("Malformed JWT. Please login again.");
		} catch (SignatureException e) {
			throw new TokenNotValidException("Malformed JWT signature.");
		} catch (ExpiredJwtException e) {
			throw new TokenNotValidException("Expired JWT. Please login again.");
		}
		
		RefreshToken refreshToken = refreshTokenRepository.findOne(claims.getId());
		
		if (refreshToken == null || refreshToken.isBlocked())
			throw new TokenNotValidException("User logged out or the refresh token had been renewed.");
		
		return refreshToken.getToken();
	}
	
	private Key getSigningKey() {
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(apiKEY);
		return new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS256.getJcaName());
	}
}