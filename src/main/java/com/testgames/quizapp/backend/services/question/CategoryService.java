package com.testgames.quizapp.backend.services.question;

import com.testgames.quizapp.backend.model.entities.Category;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface CategoryService {
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	void post(Category category);
	
	@PreAuthorize("hasRole('ROLE_USER')")
	List<Category> getAll();
	
	@PreAuthorize("hasRole('ROLE_USER')")
	Category getOne(long id);
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	void delete(long id);
}