package com.testgames.quizapp.backend.services.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.testgames.quizapp.backend.services.authentication.AuthenticationService.Provider;

public class UserAuthEntity {
	@JsonProperty
	private Provider provider;
	@JsonProperty
	private String token;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Boolean logoutAll = false;
	
	public UserAuthEntity() {
	}
	
	public Provider getProvider() {
		return provider;
	}
	
	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public Boolean getLogoutAll() {
		return logoutAll;
	}
	
	public void setLogoutAll(Boolean logoutAll) {
		this.logoutAll = logoutAll;
	}
}