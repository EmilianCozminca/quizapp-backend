package com.testgames.quizapp.backend.services.authentication;

import com.testgames.quizapp.backend.errorhandling.TokenNotValidException;

import java.io.IOException;
import java.util.Map;

public interface AuthenticationService {
	Map<String, String> login(UserAuthEntity userAuthRequest) throws IOException;
	
	Map<String, String> renewAccessToken(UserAuthEntity userAuthEntity) throws TokenNotValidException;
	
	void logout(UserAuthEntity userAuthEntity) throws TokenNotValidException;
	
	void logoutAll(UserAuthEntity userAuthEntity) throws IOException;
	
	enum Provider {
		GOOGLE_AUTH, FACEBOOK_AUTH, QuizApp_REFRESH
	}
}