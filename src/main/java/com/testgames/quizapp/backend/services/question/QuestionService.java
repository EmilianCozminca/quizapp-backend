package com.testgames.quizapp.backend.services.question;

import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.Choice;
import com.testgames.quizapp.backend.model.entities.Question;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.Set;

public interface QuestionService {
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	void postQuestion(Question question);
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	List<Question> getAll();
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	List<Question> getByDate(String from, String to) throws RequestException;
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	Question getByID(Long id);
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	void delete(long id);
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	void deleteMultiple(Set<Long> ids);
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	Set<Choice> getChoices(long id);
}
