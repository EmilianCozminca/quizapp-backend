package com.testgames.quizapp.backend.services.authentication;

class SocialLoginDetails {
	private String ID;
	private String email;
	private String displayName;
	
	String getID() {
		return ID;
	}
	
	void setID(String ID) {
		this.ID = ID;
	}
	
	String getEmail() {
		return email;
	}
	
	void setEmail(String email) {
		this.email = email;
	}
	
	String getDisplayName() {
		return displayName;
	}
	
	void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
