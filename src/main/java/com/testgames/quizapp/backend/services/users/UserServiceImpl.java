package com.testgames.quizapp.backend.services.users;

import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.Role;
import com.testgames.quizapp.backend.model.entities.User;
import com.testgames.quizapp.backend.model.repositories.RefreshTokenRepository;
import com.testgames.quizapp.backend.model.repositories.RoleRepository;
import com.testgames.quizapp.backend.model.repositories.UserRepository;
import com.testgames.quizapp.backend.services.authentication.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService {
	@Autowired
	RefreshTokenRepository refreshTokenRepository;
	@Autowired
	TokenService tokenService;
	@Autowired
	private
	UserRepository userRepository;
	@Autowired
	private
	RoleRepository roleRepository;
	
	@Override
	public User getActiveUser() {
		//TODO create non-persisted user
		User user = userRepository.findOne(Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getName()));
		
		//if (user == null)
		//	throw new RequestException("User had been deleted.");
		
		return user;
	}
	
	@Override
	public User getUserMultiPlayerStatus() throws RequestException {
		return getActiveUser();
	}
	
	@Override
	public void updateUserDetails(User user) {
		userRepository.save(user);
	}
	
	@Override
	public void deleteActiveUser() throws RequestException {
		userRepository.delete(getActiveUser());
	}
	
	@Override
	public void deleteUserAdmin(long id) throws RequestException {
		User user = userRepository.findOne(id);
		if (user == null)
			throw new RequestException("User doesn't exist");
	}
	
	@Override
	public User getUserByID(long id) {
		return userRepository.findOne(id);
	}
	
	@Override
	public User getUserByIDAdmin(long id) {
		return userRepository.findOne(id);
	}
	
	@Override
	public void setAdmin(User user) {
		Role r = roleRepository.findByName("ROLE_ADMIN");
		user.getRoles().add(r);
	}
	
	@Override
	public void removeAdmin(User user) {
		Role r = roleRepository.findByName("ROLE_ADMIN");
		user.getRoles().remove(r);
	}
	
	@Override
	public List<User> getAdmins() {
		return userRepository.findByRole("ROLE_ADMIN");
	}
}
