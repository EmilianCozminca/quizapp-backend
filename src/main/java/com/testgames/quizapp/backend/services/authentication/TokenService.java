package com.testgames.quizapp.backend.services.authentication;

import com.testgames.quizapp.backend.errorhandling.TokenNotValidException;
import com.testgames.quizapp.backend.model.entities.User;

import java.io.IOException;

public interface TokenService {
    SocialLoginDetails getFacebookDetails(String facebookToken) throws IOException;

    SocialLoginDetails getGoogleDetails(String googleToken) throws TokenNotValidException;

    String createRefreshTokenForUser(User user);

    String createAccessTokenForUser(User user);

    //Used in filter during authentication, decodes the token and adds the user id and roles in a map.
    UserAuthDetails decodeAccessToken(String encodedToken) throws TokenNotValidException;

    //Returns the owner of a specific JWT refresh token(throws if owner is deleted, token is blocked or the JWT is malformed)
    User decodeRefreshToken(String encodedToken) throws TokenNotValidException;

    //Same as above, but returns the inner token UUID2 from the JWT
    String decodeRefreshTokenToString(String encodedToken) throws TokenNotValidException;
}