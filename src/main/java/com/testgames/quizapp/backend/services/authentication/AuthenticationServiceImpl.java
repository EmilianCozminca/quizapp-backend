package com.testgames.quizapp.backend.services.authentication;

import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.errorhandling.TokenNotValidException;
import com.testgames.quizapp.backend.model.entities.RefreshToken;
import com.testgames.quizapp.backend.model.entities.User;
import com.testgames.quizapp.backend.model.repositories.RefreshTokenRepository;
import com.testgames.quizapp.backend.model.repositories.RoleRepository;
import com.testgames.quizapp.backend.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class AuthenticationServiceImpl implements AuthenticationService {
    @Autowired
    private
    TokenService tokenService;
    @Autowired
    private
    RoleRepository roleRepository;
    @Autowired
    private
    UserRepository userRepository;
    @Autowired
    private
    RefreshTokenRepository refreshTokenRepository;

    public Map<String, String> login(UserAuthEntity userAuthRequest) throws IOException {
        User user;
        SocialLoginDetails socialLoginDetails;
        boolean newUser = false;

        switch (userAuthRequest.getProvider()) {
            case FACEBOOK_AUTH:
                socialLoginDetails = tokenService.getFacebookDetails(userAuthRequest.getToken());
                user = userRepository.findByFacebookID(socialLoginDetails.getID());

                if (user == null) {
                    user = new User();
                    user.setFacebookID(socialLoginDetails.getID());

                    if (userRepository.findByEmail(socialLoginDetails.getEmail()) != null)
                        throw new TokenNotValidException("Email already in use !");

                    user.setEmail(socialLoginDetails.getEmail());

                    user.setDisplayName(socialLoginDetails.getDisplayName());
                    user.getRoles().add(roleRepository.findByName("ROLE_USER"));
                    userRepository.save(user);

                    newUser = true;
                }
                break;

            case GOOGLE_AUTH:
                socialLoginDetails = tokenService.getGoogleDetails(userAuthRequest.getToken());
                user = userRepository.findByGoogleID(socialLoginDetails.getID());

                if (user == null) {
                    user = new User();
                    user.setGoogleID(socialLoginDetails.getID());

                    if (userRepository.findByEmail(socialLoginDetails.getEmail()) != null)
                        throw new TokenNotValidException("Email already in use !");

                    user.setEmail(socialLoginDetails.getEmail());

                    user.setDisplayName(socialLoginDetails.getDisplayName());
                    user.getRoles().add(roleRepository.findByName("ROLE_USER"));
                    userRepository.save(user);

                    newUser = true;
                }
                break;

            default:
                throw new RequestException("authentication method not available");
        }

        Map<String, String> map = new HashMap<>();

        map.put("New_User", Boolean.toString(newUser));
        map.put("Refresh_Token", tokenService.createRefreshTokenForUser(user));
        map.put("Access_Token", tokenService.createAccessTokenForUser(user));

        return map;
    }

    public Map<String, String> renewAccessToken(UserAuthEntity userAuthEntity) throws TokenNotValidException {
        Map<String, String> map = new HashMap<>();
        User user;
        if (userAuthEntity.getProvider() == Provider.QuizApp_REFRESH) {
            user = tokenService.decodeRefreshToken(userAuthEntity.getToken());
        } else {
            throw new TokenNotValidException("Please check that the provider is correct");
        }

        map.put("AccessToken", tokenService.createAccessTokenForUser(user));

        return map;
    }

    public void logout(UserAuthEntity userAuthEntity) throws TokenNotValidException {
        String token;
        if (userAuthEntity.getProvider() == Provider.QuizApp_REFRESH) {
            token = tokenService.decodeRefreshTokenToString(userAuthEntity.getToken());
        } else {
            throw new TokenNotValidException("Please check that the provider is correct");
        }

        RefreshToken persistedToken = refreshTokenRepository.getOne(token);
        persistedToken.setBlocked(true);
        refreshTokenRepository.save(persistedToken);
    }

    public void logoutAll(UserAuthEntity userAuthEntity) throws IOException {
        User user;
        String thirdPartyID;
        SocialLoginDetails socialLoginDetails;

        switch (userAuthEntity.getProvider()) {
            case FACEBOOK_AUTH:
                socialLoginDetails = tokenService.getFacebookDetails(userAuthEntity.getToken());
                user = userRepository.findByFacebookID(socialLoginDetails.getID());
                break;

            case GOOGLE_AUTH:
                socialLoginDetails = tokenService.getGoogleDetails(userAuthEntity.getToken());
                user = userRepository.findByGoogleID(socialLoginDetails.getID());
                break;

            default:
                throw new TokenNotValidException("Please check that the provider is correct");
        }

        if (user == null)
            throw new TokenNotValidException("User doesn't exist or was deleted");

        Set<RefreshToken> refreshTokenList = user.getRefreshTokens();
        for (RefreshToken refreshToken : refreshTokenList) {
            refreshToken.setBlocked(true);
        }
        refreshTokenRepository.save(refreshTokenList);
    }
}