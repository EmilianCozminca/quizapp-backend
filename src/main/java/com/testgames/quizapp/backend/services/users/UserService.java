package com.testgames.quizapp.backend.services.users;

import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.User;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface UserService {
    @PreAuthorize("hasRole('ROLE_USER')")
    User getActiveUser();

    @PreAuthorize("hasRole('ROLE_USER')")
    User getUserMultiPlayerStatus() throws RequestException;

    @PreAuthorize("hasRole('ROLE_USER')")
    void updateUserDetails(User user);

    @PreAuthorize("hasRole('ROLE_USER')")
    void deleteActiveUser() throws RequestException;

    @PreAuthorize("hasRole('ROLE_USER')")
    User getUserByID(long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void deleteUserAdmin(long id) throws RequestException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    User getUserByIDAdmin(long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void setAdmin(User user);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void removeAdmin(User user);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<User> getAdmins();
}