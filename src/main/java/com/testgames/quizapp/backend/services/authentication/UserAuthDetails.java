package com.testgames.quizapp.backend.services.authentication;

import com.testgames.quizapp.backend.model.entities.Role;

import java.util.List;

public class UserAuthDetails {
	private long id;
	private List<Role> roles;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public List<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
}
