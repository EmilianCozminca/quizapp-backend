package com.testgames.quizapp.backend.services.question;

import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.Choice;
import com.testgames.quizapp.backend.model.entities.Question;
import com.testgames.quizapp.backend.model.repositories.ChoiceRepository;
import com.testgames.quizapp.backend.model.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Component
public class QuestionServiceImpl implements QuestionService {
	@Autowired
	ChoiceRepository choiceRepository;
	@Autowired
	private
	QuestionRepository questionRepository;
	
	@Override
	public void postQuestion(Question question) {
		questionRepository.save(question);
	}
	
	@Override
	public List<Question> getAll() {
		return questionRepository.findAll();
	}
	
	@Override
	public List<Question> getByDate(String from, String to) throws RequestException {
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date fromDate;
		Date toDate;
		
		try {
			fromDate = formatter.parse(from);
			toDate = formatter.parse(to);
		} catch (ParseException e) {
			throw new RequestException(e.getMessage());
		}
		
		return questionRepository.findByPostDateBetween(fromDate, toDate);
	}
	
	@Override
	public Question getByID(Long id) {
		return questionRepository.findOne(id);
	}
	
	@Override
	public void delete(long id) {
		questionRepository.delete(id);
	}
	
	@Override
	public void deleteMultiple(Set<Long> ids) {
		List<Question> list = questionRepository.findAll(ids);
		questionRepository.delete(list);
	}
	
	@Override
	public Set<Choice> getChoices(long id) {
		return questionRepository.findOne(id).getChoices();
	}
}
