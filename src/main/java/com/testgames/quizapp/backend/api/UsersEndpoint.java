package com.testgames.quizapp.backend.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.User;
import com.testgames.quizapp.backend.model.entities.Views;
import com.testgames.quizapp.backend.services.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/API/users")
class UsersEndpoint {
	private final ObjectMapper objectMapper = new ObjectMapper(); //Move to bean?
	@Autowired
	private
	UserService userService;
	
	@JsonView(Views.class)
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> getUserMultiPlayerStats() throws RequestException {
		return new ResponseEntity<>(userService.getActiveUser(), HttpStatus.OK);
	}
	
	@JsonView(Views.class)
	@RequestMapping(method = RequestMethod.PATCH, consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> updateUserDetails(@RequestBody String requestBody) throws IOException {
		
		User user = userService.getActiveUser();
		objectMapper.readerForUpdating(user).withView(Views.class).readValue(requestBody);
		userService.updateUserDetails(user);
		
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@RequestParam boolean iAmSure) throws RequestException {
		if (!iAmSure) //Prevent accidental delete
			return new ResponseEntity<>("Please confirm.", HttpStatus.EXPECTATION_FAILED);
		userService.deleteActiveUser();
		return new ResponseEntity<>("You had been deleted!.", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getUserByID", method = RequestMethod.GET)
	@JsonView(Views.class)
	public ResponseEntity<?> getUserByID(@RequestParam long id) throws RequestException {
		User user = userService.getUserByID(id);
		if (user == null)
			throw new RequestException("User doesn't exist");
		
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getByIDAdmin", method = RequestMethod.GET)
	@JsonView(Views.Extended.class)
	public ResponseEntity<?> getUserByIDAdmin(@RequestParam long id) throws RequestException {
		User user = userService.getUserByIDAdmin(id);
		if (user == null)
			throw new RequestException("User doesn't exist");
		
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAdmins", method = RequestMethod.GET)
	@JsonView(Views.Extended.class)
	public ResponseEntity<?> getAllAdmins() {
		List<User> list = userService.getAdmins();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
}