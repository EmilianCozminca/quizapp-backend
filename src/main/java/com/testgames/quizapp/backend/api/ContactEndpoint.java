package com.testgames.quizapp.backend.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.Message;
import com.testgames.quizapp.backend.model.entities.Views;
import com.testgames.quizapp.backend.model.repositories.MessageRepository;
import com.testgames.quizapp.backend.services.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.image.ImagingOpException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/API/contact")
class ContactEndpoint {
	@Autowired private MessageRepository messageRepository;
	@Autowired private	UserService userService;
	private final ObjectMapper objectMapper = new ObjectMapper(); //Move to bean?

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> postMessage(@Validated @RequestBody Message message) throws RequestException {
		message.setUser(userService.getActiveUser());
		messageRepository.save(message);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@JsonView(Views.Extended.class)
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(messageRepository.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PATCH, consumes = "application/json")
	public ResponseEntity<?> update(@RequestBody String message, @RequestParam("id") long id) throws IOException{
		JsonNode object = objectMapper.readValue(message, JsonNode.class);
		long id2 = object.get("id").longValue();

		Message messageObj = messageRepository.findOne(id2);

		objectMapper.readerForUpdating(messageObj).withView(Views.Extended.class).readValue(message);

		messageRepository.save(messageObj);

		return new ResponseEntity<>(messageObj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteMessage(@RequestParam("id") Set<Long> ids) {

		messageRepository.deleteInBatch(messageRepository.findAll(ids));
		return new ResponseEntity<>(HttpStatus.OK);
	}
}