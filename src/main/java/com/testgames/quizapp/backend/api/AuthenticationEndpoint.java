package com.testgames.quizapp.backend.api;

import com.testgames.quizapp.backend.services.authentication.AuthenticationService;
import com.testgames.quizapp.backend.services.authentication.UserAuthEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(value = "/API/authentication")
class AuthenticationEndpoint {
	@Autowired
	private
	AuthenticationService authenticationService;
	
	@RequestMapping(path = "/login", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> login(@RequestBody UserAuthEntity userAuthRequest) throws IOException {
		return new ResponseEntity<>(authenticationService.login(userAuthRequest), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/renewAccessToken", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> renewToken(@RequestBody UserAuthEntity userAuthRequest) throws IOException {
		return new ResponseEntity<>(authenticationService.renewAccessToken(userAuthRequest), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/logout", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> logout(@RequestBody UserAuthEntity userAuthRequest) throws IOException {
		if (userAuthRequest.getLogoutAll())
			authenticationService.logoutAll(userAuthRequest);
		else
			authenticationService.logout(userAuthRequest);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}