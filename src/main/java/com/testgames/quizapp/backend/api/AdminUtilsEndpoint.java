package com.testgames.quizapp.backend.api;

import com.testgames.quizapp.backend.model.repositories.MessageRepository;
import com.testgames.quizapp.backend.model.repositories.QuestionRepository;
import com.testgames.quizapp.backend.model.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/Admin/Utils")
class AdminUtilsEndpoint {
	@Autowired
	private
	UserRepository userRepository;
	@Autowired
	private
	QuestionRepository questionRepository;
	@Autowired
	private
	MessageRepository messageRepository;
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getStats() {
		
		Map<String, String> resp = new HashMap<>();
		resp.put("users", Long.toString(userRepository.count()));
		resp.put("questions", Long.toString(questionRepository.count()));
		resp.put("unapprovedQuestions", Long.toString(questionRepository.countByUnderModeration(true)));
		resp.put("unreadMessages", Long.toString(messageRepository.countByViewedByAdmin(false)));
		
		return new ResponseEntity<>(resp, HttpStatus.OK);
	}
}
