package com.testgames.quizapp.backend.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.Category;
import com.testgames.quizapp.backend.model.entities.Views;
import com.testgames.quizapp.backend.services.question.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "/API/Questions/Categories")
class QuestionsCategoryEndpoint {
	private final ObjectMapper objectMapper = new ObjectMapper();
	@Autowired
	private
	CategoryService categoryService;
	
	@JsonView(Views.Extended.class)
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<?> post(@Validated @RequestBody Category cat) {
		categoryService.post(cat);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@JsonView(Views.Extended.class)
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(categoryService.getAll(), HttpStatus.OK);
	}
	
	@JsonView(Views.Extended.class)
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getOne(@PathVariable("id") long id) throws RequestException {
		Category persisted = categoryService.getOne(id);
		
		if (persisted == null)
			throw new RequestException("Category not found");
		
		return new ResponseEntity<>(persisted, HttpStatus.OK);
	}
	
	@JsonView(Views.Extended.class)
	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH, produces = "application/json")
	public ResponseEntity<?> update(@Validated @PathVariable("id") long id, @RequestBody String cat) throws IOException {
		Category persisted = categoryService.getOne(id);
		
		if (persisted == null)
			throw new RequestException("Category not found");
		
		objectMapper.readerForUpdating(persisted).readValue(cat);
		
		categoryService.post(persisted);
		return new ResponseEntity<>(persisted, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable long id) {
		categoryService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}