package com.testgames.quizapp.backend.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.testgames.quizapp.backend.errorhandling.RequestException;
import com.testgames.quizapp.backend.model.entities.Choice;
import com.testgames.quizapp.backend.model.entities.Question;
import com.testgames.quizapp.backend.model.entities.Views;
import com.testgames.quizapp.backend.model.repositories.CategoryRepository;
import com.testgames.quizapp.backend.model.repositories.ChoiceRepository;
import com.testgames.quizapp.backend.model.repositories.QuestionRepository;
import com.testgames.quizapp.backend.services.question.QuestionService;
import com.testgames.quizapp.backend.services.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/API/Questions")
class QuestionsEndpoint {
	@Autowired
	ChoiceRepository choiceRepository;
	ObjectMapper objectMapper = new ObjectMapper(); //Move to bean?
	@Autowired
	private QuestionService questionService;
	@Autowired
	QuestionRepository questionRepository;
	@Autowired
	private
	UserService userService;

	@Autowired
	CategoryRepository categoryRepository;

	@JsonView(Views.User.class)
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<?> postQuestion(@Validated @RequestBody Question question) throws RequestException {
        question.setSubmitter(userService.getActiveUser());

        //TODO validate category ID before saving
		questionService.postQuestion(question);
		for(Choice c : question.getChoices())
			choiceRepository.save(c);

		return new ResponseEntity<>(question, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@JsonView(Views.User.class)
	public ResponseEntity<?> getAll() {
		List<Question> list = questionService.getAll();

        list = list.stream()
                .filter(p -> !p.isUnderModeration())
                .collect(Collectors.toList());

		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllExtended", method = RequestMethod.GET, produces = "application/json")
	@JsonView(Views.Extended.class)
	public ResponseEntity<?> getAllExtended() {
		List<Question> list = questionService.getAll();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@JsonView(Views.Extended.class)
	@RequestMapping(value = "/getExtendedByID", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getByID(@RequestParam("id") long id) {
		return new ResponseEntity<>(questionService.getByID(id), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(@RequestParam("id") Set<Long> ids) {
		questionService.deleteMultiple(ids);
		return new ResponseEntity<>("Successfully deleted!", HttpStatus.OK);
	}

    @JsonView(Views.Extended.class)
	@JsonIgnoreProperties(ignoreUnknown = true)
	@RequestMapping(value = "/{id}" , method=RequestMethod.PATCH, consumes="application/json")
    public ResponseEntity<?> update(@PathVariable("id") Long id ,@Validated @RequestBody String string) throws IOException , RequestException{
		//TODO fix that
		Question q = questionRepository.findOne(id);

		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.readerForUpdating(q).withView(Views.Extended.class).readValue(string);

        questionRepository.save(q);

		for(Choice c : q.getChoices())
			choiceRepository.save(c);

        return new ResponseEntity<>(q , HttpStatus.OK);
    }


    @JsonView(Views.Extended.class)
    @RequestMapping(value = "/{id}", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<?> getIndividual(@PathVariable("id") Long id) throws RequestException {
        Question question = questionRepository.findOne(id);
        return new ResponseEntity<>(question , HttpStatus.OK);
    }
	
	@JsonView(Views.Extended.class)
	@RequestMapping(value = "/{id}/Choices", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getChoices(@PathVariable("id") Long id) {
		return new ResponseEntity<>(questionRepository.findOne(id).getChoices(), HttpStatus.OK);
	}
}