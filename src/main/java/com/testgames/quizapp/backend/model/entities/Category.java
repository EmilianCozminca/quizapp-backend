package com.testgames.quizapp.backend.model.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Category {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty(access = Access.READ_ONLY)
	@JsonView(Views.Extended.class)
	private long id;
	
	@Size(min = 1, max = 30, message = "\nEnglish name must be between 1 and 30 chars!")
	@Column(nullable = false)
	@NotNull(message = "\nPlease give an English name!")
	private String nameEN;
	
	@Size(min = 1, max = 30, message = "\nRomanian name must be between 1 and 30 chars!")
	@NotNull(message = "\nPlease give an Romanian name!")
	@Column(nullable = false)
	private String nameRO;
	
	@NotNull(message = "\nPlease specific an ICO!")
	private String icon;
	
	public long getId() {
		return id;
	}
	
	public void setId(long category_id) {
		this.id = category_id;
	}
	
	public String getNameEN() {
		return nameEN;
	}
	
	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}
	
	public String getNameRO() {
		return nameRO;
	}
	
	public void setNameRO(String nameRO) {
		this.nameRO = nameRO;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
}
