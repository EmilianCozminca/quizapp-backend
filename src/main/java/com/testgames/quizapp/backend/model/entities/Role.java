package com.testgames.quizapp.backend.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;
    @JsonView(Views.Extended.class)
    @Column(unique = true, nullable = false)
    private String name;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "User_Has_Role", joinColumns = {@JoinColumn(name = "Role_id")}, inverseJoinColumns = @JoinColumn(name = "User_id"), uniqueConstraints = @UniqueConstraint(
            name = "UK1",
            columnNames = {"User_id" , "Role_id"}))
    @JsonIgnore
    private Set<User> users;

    public Role() {
    }

    public Role(String role) {
        setName(role);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
