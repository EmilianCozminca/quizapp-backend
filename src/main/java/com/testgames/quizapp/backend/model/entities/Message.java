package com.testgames.quizapp.backend.model.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Configurable
public class Message {
	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty(access = Access.READ_ONLY)
	private final Date postDate = new Date();
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty(access = Access.READ_ONLY)
	private long id;
	@ManyToOne
	@JoinColumn(name = "submitter")
	@JsonProperty(access = Access.READ_ONLY)
	private User user;
	@Column(nullable = false, columnDefinition = "NVARCHAR(MAX)")
	@NotNull(message = "Please write a title!")
	@Size(min = 1, max = 255, message = "Title should be between 1 and 255 chars long!")
	private String title;
	@Column(nullable = false, columnDefinition = "NVARCHAR(MAX)")
	@NotNull(message = "Please write a message!")
	@Size(min = 1, max = 255, message = "Message should be between 1 and 255 chars long!")
	private String message;

	@JsonView(Views.Extended.class)
	private Boolean viewedByAdmin = false;
	
	@JsonGetter(value = "user")
	public long serializeUser() {
		if(user != null)
			return user.getId();
		return -1;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Date getPostDate() {
		return postDate;
	}
	
	public Boolean getViewedByAdmin() {
		return viewedByAdmin;
	}
	
	public void setViewedByAdmin(Boolean read) {
		this.viewedByAdmin = read;
	}
}