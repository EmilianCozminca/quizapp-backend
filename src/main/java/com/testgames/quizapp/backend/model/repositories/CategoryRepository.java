package com.testgames.quizapp.backend.model.repositories;

import com.testgames.quizapp.backend.model.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
