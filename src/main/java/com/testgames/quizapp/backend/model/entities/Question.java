package com.testgames.quizapp.backend.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Question {
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty(access = Access.READ_ONLY)
    @JsonView(Views.Extended.class)
    private final Date postDate = new Date();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = Access.READ_ONLY)
    @JsonView(Views.Extended.class)
    private long id;

    @NotNull(message = "English text may not be null!")
    @Size(min = 1, max = 255, message = "English text should be between 1 and 255 chars long!")
    private String textEn;

    @NotNull(message = "Romanian text may not be null!")
    @Size(min = 1, max = 255, message = "Romanian text should be between 1 and 255 chars long!")
    private String textRo;

    @ManyToOne
    @JsonProperty(access = Access.READ_ONLY)
    @JsonView(Views.Extended.class)
    @JoinColumn(name = "submitter")
    private User submitter;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    @NotNull(message = "Category may not be null!")
    private Category category;

    @NotNull(message = "Please specify if it is a multichoice or timed question!")
    private boolean multipleChoice = true;

    @JsonView(Views.Extended.class)
    private boolean underModeration = true;

    @JsonView(Views.Extended.class)
    @NotNull(message = "Please specify if the question in MP only")
    private boolean multiplayerOnly;

    @JsonView(Views.User.class)
    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<Choice> choices = new HashSet<>();

    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<Review> reviews = new HashSet<>();

    public long getId() {
        return id;
    }

    public User getSubmitter() {
        return submitter;
    }

    @JsonProperty("submitterName")
    @JsonView(Views.Extended.class)
    public String getSubmitterName() {
        return submitter.getDisplayName();
    }

    public void setSubmitter(User submitter) {
        this.submitter = submitter;
    }

    @JsonProperty("submitter")
    public Long getSubmitterJson() {
        return submitter.getId();
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @JsonProperty("category")
    public Long getCategoryJson() {
        return category.getId();
    }

    @JsonProperty("category")
    public void setCategoryJson(long id) {
        category = new Category();
        category.setId(id);
    }

    @JsonView(Views.Extended.class)
    @JsonProperty("category_extended")
    public String getCategoryJsonExtended() {
        return category.getNameEN();
    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public boolean isUnderModeration() {
        return underModeration;
    }

    public void setUnderModeration(boolean underModeration) {
        this.underModeration = underModeration;
    }

    public Date getPostDate() {
        return postDate;
    }

    public Set<Choice> getChoices() {
        return choices;
    }

    @JsonSetter("choices")
    public void setChoices(Set<Choice> choices) {
        for (Choice e : choices) //Used during deserialization,
            e.setQuestion(this);

        this.choices = choices;
    }

    public boolean isMultiplayerOnly() {
        return multiplayerOnly;
    }

    public void setMultiplayerOnly(boolean multiplayerOnly) {
        this.multiplayerOnly = multiplayerOnly;
    }

    public void addChoice(Choice c) {
        c.setQuestion(this);
        choices.add(c);
    }

    public void removeChoice(Choice c) {
        c.setQuestion(null);
        choices.remove(c);
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    public String getTextEn() {
        return textEn;
    }

    public void setTextEn(String textEn) {
        this.textEn = textEn;
    }

    public String getTextRo() {
        return textRo;
    }

    public void setTextRo(String textRo) {
        this.textRo = textRo;
    }
}
