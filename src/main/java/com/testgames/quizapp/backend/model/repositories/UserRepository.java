package com.testgames.quizapp.backend.model.repositories;

import com.testgames.quizapp.backend.model.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	User findByGoogleID(String id);

	User findByFacebookID(String id);

	@Query("SELECT u FROM User u join u.roles r where r.name = :role ")
	List<User> findByRole(@Param("role") String role);
	
//	@Query("SELECT u FROM User u join u.refreshTokens r where r.token = :token ")
//	User geUtserByRefreshToken(@Param("token") String token);

	User findByEmail(String email);
}