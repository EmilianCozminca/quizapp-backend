package com.testgames.quizapp.backend.model.repositories;

import com.testgames.quizapp.backend.model.entities.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {
}
