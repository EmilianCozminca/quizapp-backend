package com.testgames.quizapp.backend.model.repositories;

import com.testgames.quizapp.backend.model.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
    Long countByViewedByAdmin(boolean read);
}