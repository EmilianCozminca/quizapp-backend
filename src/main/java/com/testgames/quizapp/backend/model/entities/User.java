package com.testgames.quizapp.backend.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class User {
    @Temporal(TemporalType.TIMESTAMP)
    @JsonView(Views.Extended.class)
    @JsonProperty(access = Access.READ_ONLY)
    private final Date registerDate = new Date();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = Access.READ_ONLY)
    private long id;
    @Column(unique = true) // TODO email validation
    private String email;
    @JsonIgnore
    private String password = UUID.randomUUID().toString(); //TODO better
    @JsonView(Views.Extended.class)
    @JsonProperty(access = Access.READ_ONLY)
    @Column(unique = true)
    private String googleID;
    @JsonView(Views.Extended.class)
    @JsonProperty(access = Access.READ_ONLY)
    @Column(unique = true)
    private String facebookID;
    @JsonProperty(access = Access.READ_ONLY)
    private int totalPoints;

    @JsonProperty(access = Access.READ_ONLY)
    private int wins;

    @JsonProperty(access = Access.READ_ONLY)
    private int defeats;

    private String displayName = "";

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "User_Has_Role", joinColumns = {@JoinColumn(name = "User_id")}, inverseJoinColumns = @JoinColumn(name = "Role_id"), uniqueConstraints = @UniqueConstraint(
            name = "UK1",
            columnNames = {"User_id" , "Role_id"}))
    private Set<Role> roles = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Set<RefreshToken> refreshTokens = new HashSet<>();

    public long getId() {
        return id;
    }

    public String getGoogleID() {
        return googleID;
    }

    public void setGoogleID(String googleID) {
        this.googleID = googleID;
    }

    public String getFacebookID() {
        return facebookID;
    }

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDefeats() {
        return defeats;
    }

    public void setDefeats(int defeats) {
        this.defeats = defeats;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<RefreshToken> getRefreshTokens() {
        return refreshTokens;
    }

    public void setRefreshTokens(Set<RefreshToken> refreshTokens) {
        this.refreshTokens = refreshTokens;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}