package com.testgames.quizapp.backend.model.repositories;

import com.testgames.quizapp.backend.model.entities.Choice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChoiceRepository extends JpaRepository<Choice, Long> {
    List<Choice> getByQuestionId(long id);
}
