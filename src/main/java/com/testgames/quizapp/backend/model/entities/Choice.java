package com.testgames.quizapp.backend.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;

@Entity
public class Choice {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(Views.Extended.class)
	@JsonProperty(access = Access.READ_ONLY)
	private long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "question", nullable = false)
	@JsonIgnore
	private Question question;
	
	private String answer;
	
	private boolean isCorrect;
	
	public long getId() {
		return id;
	}
	
	public Question getQuestion() {
		return question;
	}
	
	public void setQuestion(Question question) {
		this.question = question;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public boolean isCorrect() {
		return isCorrect;
	}
	
	public void setCorrect(boolean correct) {
		isCorrect = correct;
	}
}
