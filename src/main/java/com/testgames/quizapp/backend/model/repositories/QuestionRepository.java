package com.testgames.quizapp.backend.model.repositories;

import com.testgames.quizapp.backend.model.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
	List<Question> findByPostDateBetween(Date min, Date max);
	
	List<Question> findBySubmitterId(long id);
	
	List<Question> findByUnderModeration(boolean underModeration);
	
	long countByUnderModeration(boolean underModeration);
}