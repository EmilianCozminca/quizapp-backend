$(document).ready(function() {
    var table;
    var ViewModel = function() {
        var self = this;
    
        self.question = ko.observable();
        self.categories = ko.observableArray();
        self.selectedValue = ko.observableArray();

        self.test = function() {
            console.log(self.question().choices);
        }

        self.saveQuestion = function() {
            self.question().category = self.selectedValue().id;

            $.ajax({
                url: '/API/Questions/' + self.question().id,
                headers: {
                    'X-CSRFToken': $('meta[name="token"]').attr('content')
                },
                data: JSON.stringify(self.question()),
                contentType: "application/json",
                dataType: "json",
                type: 'PATCH',
                dataType: 'text',
                success: function(result) {
                    table.ajax.reload(); //TODO make more efficient
                },
                error: function(request, status, error) {
                    alert(request.responseText);
                }
            });

        }
        self.deleteQuestion = function() {
            $.ajax({
                url: '/API/Questions/?id=' + self.question().id,
                headers: {
                    'X-CSRFToken': $('meta[name="token"]').attr('content')
                },
                type: 'DELETE',
                dataType: 'text',
                success: function(result) {
                    table.ajax.reload(); //TODO make more efficient
                },
                error: function(request, status, error) {
                    alert(request.responseText);
                }
            });
        }
    };


    var avm = new ViewModel()
    ko.applyBindings(avm);

    table = $('#test').DataTable({
        "processing": true,
        "ajax": {
            "url": "/API/Questions/getAllExtended",
            "dataSrc": ""
        },

        dom: 'Bfrtip',
        buttons: [{
            text: 'New Question',
            action: function(e, dt, node, config) {

            }
        }],

        "autoWidth": false,
        "columns": [{
                "data": "textEn"
            },
            {
                "data": "textRo"
            },
            {
                "data": "category_extended" //TODO process this client side
            },
            {
                "data": "multipleChoice"
            },
            {
                "data": "underModeration"
            },
            {
                "data": "postDate"
            },
            {
                "data": "submitterName" //TODO process this client side
            },
            {
                "data": ""
            }
        ],
        "columnDefs": [{
            "targets": -1,
            "data": null,
            "defaultContent": '<button class="btn btn-info view">View</button>'
        }]
    });

    $('#test tbody').on('click', '.view', function() {
        var data = table.row($(this).parents('tr')).data();

        $.ajax({
            url: '/API/Questions/' + data.id + '/Choices',
            headers: {
                'X-CSRFToken': $('meta[name="token"]').attr('content')
            },
            type: 'GET',
            dataType: 'text',
            success: function(result) {
                data.choices = JSON.parse(result);

                var v = $.grep(avm.categories(), function(e){ return e.id == data.category; });

                avm.selectedValue(v[0]);
                avm.question(data);
            },
            error: function(request, status, error) {
                alert(request.responseText);
            }
        });

        $.ajax({
            url: '/API/Questions/Categories',
            headers: {
                'X-CSRFToken': $('meta[name="token"]').attr('content')
            },
            type: 'GET',
            dataType: 'text',
            success: function(result) {
                avm.categories(JSON.parse(result));
            },
            error: function(request, status, error) {
                alert(request.responseText);
            }
        });
        $('#messageModal').modal('show');
    });
});
