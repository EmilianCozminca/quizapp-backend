$(document).ready(function() {
    var currentId;

    var table = $('#test').DataTable({
        dom: 'Blfrtip',
        buttons: [{
            text: 'My button',
            action: function(e, dt, node, config) {
                $('#createNewCategoryModal').modal('show');
            }
        }],
        "processing": true,
        "ajax": {
            "url": "../API/Questions/Categories",
            "dataSrc": ""
        },
        "autoWidth": false,
        "columns": [{
                "data": "icon"
            },
            {
                "data": "nameEN"
            },
            {
                "data": ""
            }
        ],
        "columnDefs": [{
                "targets": 0,
                "data": "icon",
                "render": function(url) {
                    return '<img height="32" width="32" src="' + url + '"/>';
                }
            },
            {
                "targets": 2,
                "defaultContent": '<button class="btn btn-info view">View</button>'
            }
        ]
    });

    $('#test tbody').on('click', '.view', function() {
        var data = table.row($(this).parents('tr')).data();

        $('#iconURL').val(data.icon);
        $('#nameEn').val(data.nameEN);
        $('#nameRo').val(data.nameRO);

        currentId = data.id;

        $('#messageModal').modal('show');
    });

    $('#test tbody').on('click', '.delete', function() {
        var row = table.row($(this).parents('tr'));
        $.ajax({
            url: '../API/Contact/?id=' + table.row($(this).parents('tr')).data().id,
            headers: {
                'X-CSRFToken': $('meta[name="token"]').attr('content')
            },
            type: 'DELETE',
            success: function(result) {
                row.remove().draw();
            },
            error: function(request, status, error) {
                alert(request.responseText);
            }
        });
    });

    $("#uploadImg").click(function() {
        alert("Handler for .click() called.");
    });

    $("#modalSaveBtn").click(function() {
        var a = {
            id: currentId,
            nameEN: $('#nameEn').val(),
            nameRO: $('#nameRo').val()
        };
        a = JSON.stringify(a);
        request = $.ajax({
            url: "../API/Questions/Categories/" + currentId,
            type: 'PATCH',
            contentType: 'application/json',
            data: a,
            success: function(result) {
                $('#messageModal').modal('hide');
            },
            error: function(request, status, error) {
                alert("Something went wrong.");
            }
        });
    });

    $("#modalSaveBtnNew").click(function() {
        var a = {
            id: currentId,
            nameEN: $('#nameEnNew').val(),
            nameRO: $('#nameRoNew').val(),
            icon: $('#iconURLNew').val()
        };
        a = JSON.stringify(a);
        request = $.ajax({
            url: "../API/Questions/Categories/",
            type: 'POST',
            contentType: 'application/json',
            data: a,
            dataType: 'text',
            success: function(result) {
                $('#createNewCategoryModal').modal('hide');
            },
            error: function(request, status, error) {
                var msg = jQuery.parseJSON(request.responseText);
                if (typeof msg.message != 'undefined')
                    alert(msg.message);
                else
                    alert(msg.errors);
            }
        });
    });


    $("#modalDeleteBtn").click(function() {
        var a = {
            id: currentId,
            nameEN: $('#nameEn').val(),
            nameRO: $('#nameRo').val()
        };
        a = JSON.stringify(a);
        request = $.ajax({
            url: "../API/Questions/Categories/" + currentId,
            type: 'DELETE',
            success: function(result) {

                $('#messageModal').modal('hide');
            },
            error: function(request, status, error) {
                alert("Something went wrong.");
            }
        });
    });

});
