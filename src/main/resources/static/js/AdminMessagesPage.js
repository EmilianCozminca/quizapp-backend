var table;
var avm;

ViewModel = function() {
    var self = this;
    self.message = ko.observable();

    self.deleteMessage = function() {
        $.ajax({
            url: '/API/contact/?id=' + self.message().id,
            headers: {
                'X-CSRFToken': $('meta[name="token"]').attr('content')
            },
            type: 'DELETE',
            success: function(result) {
                table.ajax.reload(); //TODO make more efficient
            },
            error: function(request, status, error) {
                alert(request.responseText);
            }
        });
    }

    self.toggleMessage = function(){
        var valueToBeSent = {
            id: self.message().id,
            viewedByAdmin: !self.message().viewedByAdmin
        };

        $.ajax({
            url: '/API/contact/?id=' + self.message().id,
            data: JSON.stringify(valueToBeSent),
            contentType: "application/json",
            dataType: "json",
            headers: {
                'X-CSRFToken': $('meta[name="token"]').attr('content')
            },
            type: 'PATCH',
            success: function(result) {
                table.ajax.reload(); //TODO make more efficient
            },
            error: function(error) {
                alert(error);
            }
        });
    }
};

$(document).ready(function() {
    avm = new ViewModel()
    ko.applyBindings(avm);

    table = $('#test').DataTable({
        "processing": true,
        "ajax": {
            "url": "/API/contact",
            "dataSrc": ""
        },
        "autoWidth": false,
        "columns": [{
                "data": "title"
            },
            {
                "data": "postDate"
            },
            {
                "data": "viewedByAdmin"
            },
            {
                "data": ""
            }
        ],
        "columnDefs": [{
            "targets": -1,
            "data": null,
            "defaultContent": '<button class="btn btn-info view">View</button>'
        }]
    });

    $('#test tbody').on('click', '.view', function() {
        avm.message(table.row($(this).parents('tr')).data());
        $('#messageModal').modal('show');
    });
});
