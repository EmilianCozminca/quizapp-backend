package com.testgames.quizapp.backend;

import com.testgames.quizapp.backend.model.entities.Choice;
import com.testgames.quizapp.backend.model.entities.Question;
import com.testgames.quizapp.backend.model.repositories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Random;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class Test1 {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private ChoiceRepository choiceRepository;

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;


    @Autowired
    private ReviewRepository reviewRepository;

    @Test
    public void exampleTest() {
        Random r = new Random();
        int Low = 1;
        int High = 3;

        for(int i = 0 ; i < 200 ; i++){
            long Result = r.nextInt(High-Low) + Low;

            Question question = new Question();
            question.setCategory(categoryRepository.findOne(Result));
            question.setSubmitter(userRepository.findOne(7L));
            question.setTextEn(UUID.randomUUID().toString());
            question.setTextRo(UUID.randomUUID().toString());

            boolean b = r.nextInt() % 2 == 0;
            question.setMultipleChoice(b);

            Choice choice = new Choice();
            choice.setAnswer(UUID.randomUUID().toString());
            choice.setCorrect(true);
            choice.setQuestion(question);
            question.getChoices().add(choice);

            if(b) {
                for (int j = 0; j < 10; j++) {
                    Choice choice2 = new Choice();
                    choice2.setAnswer(UUID.randomUUID().toString());
                    choice2.setCorrect(false);
                    choice2.setQuestion(question);
                    question.getChoices().add(choice2);
                }
            }

            questionRepository.save(question);
            choiceRepository.save(question.getChoices());
        }
    }

}