This is a Spring-Boot application for an upcoming Android game.
It can store users and authenticate them with Google and Facebook, different questions and their answers, reviews for the questions and user messages for the admins.
It haves a Web interface from which administrators can view the database.

The user can **create an account with Google or Facebook** and , then, the user receives an **Access Token** used for authentication and a **Refresh Token**.

Technologies used :

* Java 8 
* Spring Boot 
* JPA 
* Thymeleaf 
* JSON Web Token
* AJAX
* Bootstrap
* JQuery
* JQuery Datatables
* MySQL
* Knockout JS

For now, I plan to complete the backend before moving on to the Android game.